package ru.t1.kruglikov.tm.api.controller;

public interface ICommandController {

    void displayInfo();

    void displayArguments();

    void displayCommands();

    void displayHelp();

    void displayVersion();

    void displayAbout();

}
