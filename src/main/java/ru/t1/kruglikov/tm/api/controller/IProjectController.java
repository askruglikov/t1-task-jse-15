package ru.t1.kruglikov.tm.api.controller;

import ru.t1.kruglikov.tm.model.Project;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

    void showProjectById();

    void showProjectByIndex();

    void showProject(Project project);

    void updateProjectByIndex();

    void updateProjectById();

    void removeProjectById();

    void removeProjectByIndex();

    void changeProjectStatusByIndex();

    void changeProjectStatusById();

    void startProjectByIndex();

    void startProjectById();

    void completeProjectByIndex();

    void completeProjectById();

}
