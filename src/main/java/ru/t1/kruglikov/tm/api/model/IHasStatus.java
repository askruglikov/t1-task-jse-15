package ru.t1.kruglikov.tm.api.model;


import ru.t1.kruglikov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}

